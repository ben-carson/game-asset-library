# Distributed Assets
There's no need to download all assets to keep them here. Just creating a list of links to the various places where I've purchased assets

### The List
1. [gamedev market](https://www.gamedevmarket.net/user/orders/)
1. [Unity Store](https://assetstore.unity.com/account/assets)
