# Game Asset Library
I'm learning about gamedev and how quickly a project's filesize can balloon to *very* large sizes.<br>
As a means to mitigate this inevitable situation, I'm transferring all of the free gamedev assets I've collected from an actively developed project to a standalone project. One that will house all of my the assets I collect and create. This creates a convenient one-stop-shop for any needs I may have in the future without having to branch and merge these large files and file histories unnecessarily. 

# Links to other, off-repo resources
## Audio
---
### Music
* [Purple Planet](http://www.purple-planet.com/) - Royalty-Free Music
* https://filmstro.com/music
* [Antti Luode](https://www.reddit.com/r/gamedev/comments/89irke/i_have_released_my_1641_instrumental_pieces_free/) - 1641 instrumental pieces, CC BY 3.0
* [Antti Luode](https://www.reddit.com/r/gameassets/comments/dbmrxo/i_have_released_my_2028_instrumental_pieces_free/) - 2048 instrumental pieces, CC BY 3.0
* This is the [SAMPLER](https://www.pmsfx.com/pmsfxsampler) that is included in this repo
* [Holographic Food](https://pastafriend.bandcamp.com/album/holographic-food) - SynthWave
* [Psych Ward Media](https://soundcloud.com/psychwardmedia) Soundcloud - Synth music producer
* [xaxAttax](https://soundcloud.com/xaxattax) - Soundcloud for Open Source Game Beats/Loops
* [Punch Deck](https://soundcloud.com/punch-deck/tracks) - Soundcloud for some CC-BY-3.0 electronic and orchestral music
* [Clair Poirier](https://www.reddit.com/r/gameassets/comments/8stc00/i_have_sci_fi_vg_music_if_anyone_is_interested/) - CC0, Sci-fi inspired music
* [Gravity Sound](https://www.youtube.com/channel/UCQ7Xmyu5eXpJfkEoMtRMv1w/playlists?disable_polymer=1) - CC4 licensed chill/hiphop/soundscape/electronic music
* [Otis MacDonald](https://soundcloud.com/otismacmusic) - Joe Bagale released 30 tracks under a copyright free license on Youtube Music Library
* [Bjornar Kibsgaard](https://bjokib.com/index.php) - Electronic music with a [CC4.0](https://bjokib.com/license-information.php) or [paid](https://bjokib.com/license-information.php#standardlicense) license
* [iDev](https://assets.idevgames.co.uk/vendor/idev-games/) - small sample of decent music variety
* [SignatureSamples](https://www.reddit.com/r/gameassets/comments/bo4fs0/here_is_5gb_of_royalty_free_sounds_i_have_created/) - free music and sounds CC2.0 or purchase for commercial license
* [r/gameassets - Sounds](https://www.reddit.com/r/gameassets/search?q=flair%3ASounds&restrict_sr=on)
* [iDevGames - music](https://assets.idevgames.co.uk/browse/) - small collection of free-to-use game menu music
* [James Hammond's Bandcamp](https://jameshammondrf.bandcamp.com/album/video-game-inspired-songs-and-loops-royalty-free) - Royalty-free, but unknown licensing. _Be careful._
* [Low Poly Loops](https://kentkercher.bandcamp.com/album/low-poly-loops-volume-one-rowdy-romplers) from Kent Kercher - Creative Commons CC-BY license
* [Sound Spect](https://soundspect.com/) - Music and loops, organized by mood and/or genre, free for non-commercial, pay-per-track for commercial, see [licensing](https://soundspect.com/licencing); user account required to be create for downloads; *3 free download per day limit, 10 downloads per week*
* [Llydia Lancaster](https://soundcloud.com/lewislancastermusic) - royalty-free and free downloads
* [One Man Symphony](https://onemansymphony.bandcamp.com/releases) - 80+ tracks of CC BY 4.0 licensed synthy music
* [Void1 Gaming](https://www.void1gaming.com/music) - mix of free and cheap music
* [Audio Creature](https://assetstore.unity.com/publishers/54535) - free cyberpunk noir and "mystical" soundtracks as Unity Assets
* [LogLog Games](https://soundcloud.com/loglog-games) - *good* free combat music loops

#### Sounds
* [ToneDock](https://www.tonedock.com/samplepacks/6_) UI Sounds - free
* [Free To Use Sounds](https://freetousesounds.com/) looks like [CC0](https://freetousesounds.com/about/the-license-agreement/) sounds
* [SONNISS.com freebie](https://sonniss.com/gameaudiogdc18/) for GDC 2018 (30Gb+ download)
* [Fire Arms SFX](http://freefirearmsfx.com/the-library/4582574710)
* [Bfxr](https://www.bfxr.net/) - make sound effects for computer games
* [GMXR](https://wubs.itch.io/gmxr) - Name-Your-Price tool to build simple sounds from a waveform
* [Bloma - DS13 album](https://bloma.bandcamp.com/album/ds13) - CC4 license, lo-fi electro album with loops
* [PunchDeck](https://www.reddit.com/r/gameassets/comments/921gq2/music_free_orchestral_electonic_songs_and/) - CC BY 3.0 license, orchestral & electronic songs and seemless loops
* [NichiMusic blog](https://ninichimusic.com/blog/2017/3/29/12-great-places-to-find-sound-effects-for-your-game-or-project) - *12 Places to Find Sounds*, great post with writeups of each resource
* [PMSFX Sampler](https://www.pmsfx.com/pmsfxsampler) - Sampler of HQ sound effects from across PMSFX packs. The [license](https://www.pmsfx.com/legal-1) is attribution-based
* [Aturax Audio](https://www.aturaxaudio.com/file-share) - free foley sound effects, CC Attribution License
* [ShapeForms Audio](https://shapeforms.itch.io/shapeforms-audio-free-sfx) - free sample pack of full sounds packs available, CC0 Licensed.

## Art
---
* [tldevtech blog](https://www.tldevtech.com/game-assets-for-ios-and-android-game-development/) - meta collection of paid & free 2D & 3D art for games
  * **TODO**: parse this blog to integrate it's resources into this list
### 2D Art
#### Pixel Art
* [Ansimuz](https://ansimuz.itch.io/) - retro paid and free high-quality pixel art (characters, env, effects, props)
* [Pixel Game Art](http://pixelgameart.org/web/)
* [Open Game Art](http://opengameart.org/)
* [Free Game Arts](http://freegamearts.tuxfamily.org/)
* [kronbits](https://kronbits.itch.io/) Mostly pixel art, some textures
* [Itch.io: Free Assets](https://itch.io/game-assets/free) - Mostly pixelart
* [Rotting Pixels](https://rottingpixels.itch.io/) - CC0 2D platformer tilesets
* [Timber Wold Games](https://timberwolfgames.itch.io/) - CC0 sprite packs
* [Pixel Frog](https://pixel-frog.itch.io/pixel-adventure-1) - Pixel Adventure 1 free asset pack, seems to be CC0
* [Stealthix](https://stealthix.itch.io/) - CC0 Pixel Artist, not all is free
* [Jamie Cross](https://jamiecross.itch.io/) - CC0 free and paid 2D GUI kits and tilesets
* [Pixel Art Guns](https://gg-undroid-games.itch.io/) - CC0 type license for 2D Pixel guns with animations
* [Void1 Gaming](https://www.void1gaming.com/graphics) - CC0 type license for lots of art

#### Vector Art
* [Open Game Graphics](https://opengamegraphics.com/)

#### Icons, Fonts, and UI Elements
* [game-icons](https://game-icons.net)
* [OpenGameGraphics UI](https://opengamegraphics.com/product-category/game-ui-assets/)
* [LoSpec Palette List](https://lospec.com/palette-list) - collection of curated palettes
* [/r/gameassets - Icons](https://www.reddit.com/r/gameassets/search?q=flair%3AIcons&restrict_sr=on)
* [Fjell Maxim - reticles](https://fjellmaxim.com/goodies) - reticles in SVG format
* [GGBot Fonts](https://ggbot.itch.io/ggbot-font-pack) - 9 free fonts from GGBot on itch
* [Font Squirrel](https://www.fontsquirrel.com/) - free fonts
* [Google Fonts](https://fonts.google.com/) - free & open source fonts
* [Crunchycons](https://spoicat.itch.io/crunchycons) - CC0 handwritten style font
* [LadyLuck](https://ladyluck.itch.io/) - free iconsets

### 3D Art
#### 3D Models
* [The Base Mesh](https://thebasemesh.com/) (CCO license) - simple starting objects for prototyping
* [NASA JPL models](https://nasa3d.arc.nasa.gov/models)
* [3DShed](https://3dshed.wordpress.com/) (CC0 license) [low-poly]
* [Khalkeus' 3D Art Pack](http://www.procjam.com/art/khalkeus.html) from PROCJAM 2016 (CC Attribution-NonCommercial) [low-poly]
* [DevilsGarage 3D Art Pack](http://www.procjam.com/art/devil.html) from PROCJAM 2017 (CC A-NC license) [low-poly]
* [Quaternius](http://quaternius.com/assets.html) (CC0 license) [low-poly]
* [12 fruits & Veg](https://gumroad.com/l/frutsvegetables) - pay-what-you-want, 4K PBR textures
* [Low Poly Forest](https://devilsworkshop.itch.io/lowpoly-forest-pack) - (CC 1.0 Universal)
* [VimanaVoid](https://www.patreon.com/vimanavoid) - Some indie named Jav. No license on their work, use at your own risk.
* [godgoldfear](https://godgoldfear.itch.io/) - someone sharing CC0 low-poly rock and tree models
* [Oozymandias](https://drive.google.com/drive/folders/1pighV6B2wk1Kn6eap6RfuRaqT0D2ObPB) - unlicensed (_beware_) spaceship models, has a [Patreon](https://www.patreon.com/0ozymandias)
* [Dima](https://n0madcoder.itch.io/dima-game-ready-character-base-mesh) - name-your-price untextured 3d human model
* [SyntyStore](https://syntystore.com/pages/free-assets) - free low-poly assets from synty store
* [Modular Slums](https://sketchfab.com/3d-models/modular-slums-e2b6f532e1fe4f7280890971d78183f4) - CC Attribution from [Lex](https://sketchfab.com/lexferreira89), has other CC art
* [3DHavens](https://3dmodelhaven.com/) - small, but growning, collection of [CCO](https://3dmodelhaven.com/p/license.php) 4K PBR models
* [Low Poly Sci-Fi Greeble](https://siris-pendrake.itch.io/low-poly-greeble-pack) - small name-your-own-price asset pack
* [Low Poly Soldier Character mesh](https://n0madcoder.itch.io/nikita-low-poly-character-base-mesh) - name-your-own-price character model, no texture, no rig
* [Voxel Gun Pack](https://erick1310.itch.io/gun-pack) - free voxel guns with materials
* Voxel Vehicles [1](https://theodd1in.itch.io/vehicles) and [2](https://theodd1in.itch.io/vehicles2) - name-your-price cutsy voxel cars; artist has other voxel art too
* [MB Labs](https://mblab.dev/) - Humanoid model generator for Blender 2.8+, *be careful*, released under [AGPLv3 license](https://mblab.dev/license/)
* [MakeHuman](http://www.makehumancommunity.org/) - open source tool for making 3D humans - [AGPLv3 license](http://www.makehumancommunity.org/content/license_explanation.html), however officially exported models are [CC0](http://www.makehumancommunity.org/wiki/FAQ:Can_I_sell_models_created_with_MakeHuman%3F), they just don't want people using MH as a library inside some other character generator
* [Max Parata](https://maxparata.itch.io/) - free voxel-based asset packs, includes, planes, mechas, humans, environments, etc
* [chewycereal](https://chewycereal.itch.io/) - free low-poly spaceship pack
* [SAVAGE Research Group](https://savage.nps.edu/Savage/#information) - military models of varying quality in X3D format with [CC](https://savage.nps.edu/Savage/license.html) ??? license
   * [Delta3d Asset Library](https://sourceforge.net/projects/delta3d/files/delta3d-asset-library/) - REAAALLLYY old military models that go with the Delta3D engine, related to SAVAGE assets I believe
* [FreeGameAsset.com](https://www.freegameasset.com/) - high quality PBR barrels, not much else
* [3D Melee Weapons](https://elijahcobden.itch.io/3d-melee-weapon-pack) - 2K & 4K textured medieval weapons, CC0 license, I think.
* [Free3DCG.com](https://free3dcg.com/) - single artist giving away 3D models with a variety of categories, unknown license.
* [3dmodelscc0.com](https://www.3dmodelscc0.com/3dmodels) - random assortment of models with 2K & 4K textures, assuming cc0 license
* [loafbrr](https://loafbrr.itch.io/) - free low-poly models, decent modular buildings and roads
* [Creative Trio](https://creativetrio.art/) - *beautiful* low-poly plants, stone bridges, and wood furniture and doors
* [Zsky2000](https://zsky2000.itch.io/) - lots of free low-poly assets
* [OOP5](https://oop5assets.gumroad.com/) - some free low-poly environmental assets
* [Alstra Infinite](https://alstrainfinite.itch.io/) - free & cheap low-poly environmental, vehicle, boat, plane assets
* [TastyTony](https://sketchfab.com/TastyTony) - low-poly but good detail real guns, [CC by 4.0](https://creativecommons.org/licenses/by/4.0/) license (attribution reqd)
* [herio](https://herio.itch.io/) - very free low-poly low-rez playset packs

#### Textures
* [Share Textures](https://www.sharetextures.com/) - crowd-funded [CC0](https://www.sharetextures.com/about)-licensed 4K textures
* [CC0 Textures](https://cc0textures.com/home#_=_)
* DevAssets: [General Textures](http://devassets.com/assets/general-textures/)
* [TurboSquid textures](https://www.turbosquid.com/Textures) 
* [Poliigon](https://www.poliigon.com/) (commercial)
* [3Dtextures.me](https://3dtextures.me/) (CC0 license)
* [HDRIHaven](https://hdrihaven.com/hdris/) (CC0 license)
* [TextureHaven](https://texturehaven.com/textures/) (CC0 license)
* [Quixel Megascans](https://megascans.se/library/free) (Free section) PBR photogrammetry - used by Unity in Book of the Dead demo
* [Texture Ninja](https://texture.ninja/) - CC0 textures
* [texturelib](http://texturelib.com/) - large collection of [CC0](http://texturelib.com/license/) textures
* [Pixar Free Texture Pack](https://community.renderman.pixar.com/article/114/library-pixar-one-twenty-eight.html) - CC4 licensed textures from Pixar, circa 1993
* [Blender Cloud](https://cloud.blender.org/p/textures/) - [CC0 licensed](https://cloud.blender.org/p/textures/about) textures from Blender Open Movie projects
* [CGBookcase.com](https://www.cgbookcase.com/textures/) - [CC0 licensed](https://www.cgbookcase.com/license-information) PBR textures
* [JulioVII](https://juliovii.itch.io/) - CC-BY licenced free PBR texture pack with other paid options

#### Texture Tools
* [NormalMapOnline](https://cpetry.github.io/NormalMap-Online/) - online tool for generating normal maps
* [Materialize](http://www.boundingboxsoftware.com/materialize/index.php) - Win only - Generate materials from images
** [Materialize forks](https://github.com/BoundingBoxSoftware/Materialize/issues/6#issuecomment-447875509) - users have created Linux and OS X forks of the project, found in the issue tracker
* [TextureLab](https://njbrown.itch.io/texturelab) - Free LGPLv3 procedural texture generator - [source](https://github.com/njbrown/texturelab)
* [Material Maker](https://rodzilla.itch.io/material-maker) - Free MIT license - [source](https://github.com/RodZill4/material-maker)
* [TexGraph](https://galloscript.itch.io/texgraph) - unknown license texture generator - [source](https://github.com/galloscript/TexGraph-Public)
* [Imogen](https://github.com/CedricGuillemet/Imogen)(source) - MIT Licensed GPU Texture generator

#### 3D Search
* [Yeggi](https://yeggi.com/)
* [Poly](https://poly.google.com/)
* [Free3D](https://free3d.com/)
* [Yobi3D](https://www.yobi3d.com/#!/)
* [TurboSquid](https://www.turbosquid.com/Search/3D-Models/free) (free)
* [BlendSwap](https://www.blendswap.com/)
* [Remix 3D](https://www.remix3d.com/discover?section=34b78f58881242e4ab611e4ab5ffaa78)
* [Clara.io](https://clara.io/library)
* [archive3d.net](https://archive3d.net/)
* [CGTrader](https://www.cgtrader.com/free-3d-models) - free models, check licenses carefully before using in commercial product
* [OpenGameArt 3D](https://opengameart.org/art-search-advanced?keys=&field_art_type_tid%5B%5D=10&sort_by=count&sort_order=DESC) - [license](https://opengameart.org/content/faq#q-how-to-credit) varies by artist, so look carefully
* [r/gameassets - 3D models](https://www.reddit.com/r/gameassets/search?q=flair%3A3D&restrict_sr=on) 
* [3DMDB](https://3dmdb.com)


#### 3D Tools
* [Blender](https://www.blender.org/) Win, Mac, & Linux, FOSS 3D modeling app
* [MagicaVoxel](https://ephtracy.github.io/)
* [Sculptris](http://pixologic.com/sculptris/)
* [VRoid](https://vroid.pixiv.net/) - web-based modeling
* [MeshRoom](https://alicevision.github.io/#meshroom) - Windows and Linux - open source photogrammetry software
* [Space Skybox Generator](http://wwwtyro.github.io/space-3d) - [Rye Terrell](https://github.com/wwwtyro)'s awesome procedural spacefield generator, but no way to export :(
* [Planet Generator](https://wwwtyro.github.io/procedural.js/planet1/) - Procedural 3D planet texture generator with [Unlicense](https://github.com/wwwtyro/procedural.js/blob/gh-pages/LICENSE) by [Rye Terrell](https://github.com/wwwtyro)
* [Effekseer](https://effekseer.github.io/en/index.html) - [MIT Licensed](https://github.com/effekseer/Effekseer/blob/master/LICENSE) particle generator tool
* [Talos](https://talosvfx.com/) - [Open Source](https://github.com/rockbite/talos), [Apache 2.0](https://github.com/rockbite/talos/blob/master/LICENSE) licensed particle shader written for libGDX
* [Mocap Data](http://mocapdata.com/) - Motion Capture data, not sure of licensing yet
* [Namco Bandi MoCap Datasets](https://github.com/BandaiNamcoResearchInc/Bandai-Namco-Research-Motiondataset) (CC BY-NC-ND 4.0) non-commercial- mocap data in bvh file format
* [Blender-osm](https://github.com/vvoovv/blender-osm/wiki/Import-OpenStreetMap-(.osm)) - Import OpenStreetMap export data into Blender
* [Road Architect](https://github.com/MicroGSD/RoadArchitect/releases) - professional quality road system creator 
* [SculptGL](https://stephaneginier.com/sculptgl/) - web-based sculpting program
* [TreeIt](https://www.evolved-software.com/treeit/treeit) from [Evolved Software](http://www.evolved-software.com/) - Free Windows application with "As Is" license for creating customizable 3D trees
* [Spaceship Generator](https://github.com/ldo/blender_spaceship_generator) [MIT licensed] Blender plugin that procedurally generates textured spaceships, generated models are CC 3.0 licensed
* [Dust3D](https://dust3d.org/) [MIT licensed] FOSS 3D modeling and rigging tool

### Miscellaneous
* [Keijiro Takahashi github](https://github.com/keijiro)
* [The Blueprints](http://the-blueprints.com/)
* [Markov Name Generator](http://www.samcodes.co.uk/project/markov-namegen/)
* [terrain.party](https://terrain.party) - real-world height map generator
* [Pixabay](https://pixabay.com/) - CC0 images
* [A-Frame](https://aframe.io/) - Mozilla-developed framework for WebVR
* [kenny.nl](https://kenny.nl) - Kenny, Tons of free game assets, 3D, 2D, etc Great stuff for prototyping!
* [Smithsonian OpenAccess Images](https://www.si.edu/openaccess) - Millions of CC0 images from the Smithsonian for whatever purpose you need.
* [Procedural City Generator](https://probabletrain.itch.io/city-generator) - Generate CC0 maps for cities with 3D export possible, detailed instructions [here](https://maps.probabletrain.com/#/)
* [Void1 Gaming Scripts](https://www.void1gaming.com/scripts) - free Unity scripts for various effects

### Research
* [Game Assets](https://www.reddit.com/r/gameassets/) - gameassets subreddit
* [Unity Assets](https://www.reddit.com/r/UnityAssets/) - Unity Assets subreddit
* [Game Dev](https://www.reddit.com/r/gamedev/) - gamedev subreddit
* [GDC Vault](https://www.gdcvault.com/free) - Game Developer Conference archive of talks and lectures
* [Game Developer Magazine Vault](https://www.gdcvault.com/gdmag)
* [Games From Scratch](https://www.gamefromscratch.com/)
* [Riot Games Engineering Blog](https://engineering.riotgames.com/)
* [Big List of Postmortems](http://www.pixelprospector.com/the-big-list-of-postmortems/)
* Riot Games [Game Design Course](https://www.riotgames.com/en/urf-academy/the-curriculum) - CC 4.0 license
* **Reference**
  * [British Museum](https://www.britishmuseum.org/collection) - *Massive* collection of images, most CC 4.0 licensed

### Unity Assets
* [Unity3D Free 3D search](https://assetstore.unity.com/categories/3d/characters/?k=price%3A0&order_by=relevance&q=category%3A22&q=price%3A0&rows=42&sc=3d) - quick link to all FREE 3D content in Unity's Asset Store
* [Asset Street](https://www.221b-asset-street.com/free-3d-models.html) - Curated list of free assets from Unity Asset Store
* [Delight](https://delight-dev.github.io/) - FOSS UI framework for Unity
* [Alchemy Studio](https://assetstore.unity.com/packages/audio/music/free-music-and-sfx-collection-4369) - Unity Music asset pack
* [FREE Music Loops Collection](https://assetstore.unity.com/packages/audio/music/free-music-loops-collection-70967) - For Unity, themes: Action, battle, casual & abstract, cinematic, combat, fantasy & adventure, mystical, rock, and space adventure
* [Simple Camera Shake](https://leftshoe18.itch.io/simple-camera-shake-for-unity) - Implement basic camera shake in Unity
* [Fast Fourier Transform ocean wave simulation](https://github.com/gasgiant/FFT-Ocean) - MIT licensed prototype ocean simulation

### Open Source Games
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>code repo</th>
            <th>License</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="https://www.rigsofrods.org/">Rigs of Rods</a></td>
            <td>3D</td>
            <td><a href="https://github.com/RigsOfRods/rigs-of-rods">github</a></td>
            <td><a href="https://github.com/RigsOfRods/rigs-of-rods#license-of-rigs-of-rods">GPLv3</a>
        </tr>
        <tr>
            <td><a href="https://supertuxkart.net/Main_Page">SuperTuxKart</a></td>
            <td>3D</td>
            <td><a href="https://github.com/supertuxkart/stk-code">github</a></td>
            <td><a href="https://github.com/supertuxkart/stk-code/blob/master/COPYING">varies</a>
                <ul>
                    <li>GPL</li>
                    <li>CC-BY</li>
                    <li>CC-BY-SA</li>
                    <li>Public Domain</li>
                    <li>See 'licenses.txt' file in subdirs for more details</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td rowspan=2><a href="http://dev.ryzom.com/">Ryzom</a></td>
            <td rowspan=2>3D</td>
            <td rowspan=2><a href="https://github.com/osgcc/ryzom">github</a></td>
            <td>code - <a href="https://github.com/osgcc/ryzom/blob/master/COPYING">GPL v3</a></td>
        </tr>
        <tr>
            <td>art - <a href="https://en.wikipedia.org/wiki/Creative_Commons_license">CC</a></td>
        </tr>
        <tr>
            <td><a href="">OpenDungeons</a></td>
            <td>3D</td>
            <td><a href="https://github.com/OpenDungeons/OpenDungeons">github</a></td>
            <td><a href="https://github.com/OpenDungeons/OpenDungeons/blob/development/LICENSE.md">GPL v3</a> & <a href="https://github.com/OpenDungeons/OpenDungeons/blob/development/CREDITS">others</a></td>
        </tr>
        <tr>
            <td rowspan=2><a href="https://osslugaru.gitlab.io/">Lugaru</a></td>
            <td rowspan=2>3D</td>
            <td rowspan=2><a href="https://gitlab.com/osslugaru/lugaru">gitlab</a></td>
            <td>code - <a href="https://gitlab.com/osslugaru/lugaru">GPLv2+</a></td>
        </tr>
        <tr>
            <td>assets - <a href="https://gitlab.com/osslugaru/lugaru/blob/master/CONTENT-LICENSE.txt">varies</a><td>
        </tr>
        <tr>
            <td><a href="https://xonotic.org/">Xonotic</a></td>
            <td>3D</td>
            <td><a href="https://gitlab.com/xonotic/xonotic">gitlab</a></td>
            <td><a href="https://gitlab.com/xonotic/xonotic/blob/master/GPL-3">GPLv3</a></td>
        </tr>
        <tr>
            <td><a href="https://wz2100.net/">Warzone2100</a></td>
            <td>3D</td>
            <td><a href="https://github.com/Warzone2100/warzone2100">github</a></td>
            <td><a href="https://github.com/Warzone2100/warzone2100/blob/master/COPYING">GPLv2</a></td>
        </tr>
        <tr>
            <td><a href="http://www.netpanzer.info/">netPanzer</a></td>
            <td>2D</td>
            <td><a href="https://sourceforge.net/p/netpanzerfp/code/HEAD/tree/">SourceForge</a></td>
            <td><a href="http://www.netpanzer.info/netpanzer.info/documents/gpl-2.0.txt"></a>GPLv2</td>
        </tr>
        <tr>
            <td><a href="http://www.scorched3d.co.uk/">Scorched3D</a></td>
            <td>3D</td>
            <td><a href="https://sourceforge.net/p/scorched3d/git/ci/master/tree/">SourceForge</a></td>
            <td><a href="https://sourceforge.net/p/scorched3d/git/ci/master/tree/scorched/COPYING">GPL...1?</a></td>
        </tr>
        <tr>
            <td><a href="https://www.themanaworld.org/">The Mana World</a></td>
            <td>2D</td>
            <td>Seems to be a private projects on <a href="https://gitlab.com/themanaworld">GitLab</a>. But some mirror info is available on <a href="https://github.com/themanaworld">GitHub</a>.</td>
            <td>Unknown, possible GPLv3</td>
        </tr>
        <tr>
            <td><a href="https://play0ad.com/">0 AD</a></td>
            <td>3D</td>
            <td><a href="https://trac.wildfiregames.com/browser">SVN???</a></td>
            <td><a href="https://sourceforge.net/projects/zero-ad/">Code under GPL2, Artwork under CC-BY-SA v3</a></td>
        </tr>
    </tbody>
</table>

More to add:
* Glest - https://sourceforge.net/projects/glest/
* Megaglest - https://sourceforge.net/projects/megaglest/
* Endless Sky - https://sourceforge.net/projects/endless-sky.mirror/
* Terasology - https://sourceforge.net/projects/terasology.mirror/
* Battle for Wesnoth - https://sourceforge.net/projects/wesnoth/
* Freeciv - https://sourceforge.net/projects/freeciv/
* Cataclysm - https://sourceforge.net/projects/cataclysm.mirror/
* UFO: Alien Invastion - http://ufoai.ninex.info/wiki/
* **Goodness!** so many games are available! : https://sourceforge.net/directory/games/games/os:linux/
